#!/usr/bin/env bash

set -e

### Recompile requirements. Run this when you need to
### add/remove requirements on the input files.
###
### Usage:
###   ./recompile.sh

ROOT_DIR="$(dirname "$(readlink --canonicalize "$0")")"
readonly ROOT_DIR

function log_fatal {
  >&2 echo "$@"
  exit 1
}

function main {
  if ! command -v python3.8; then
    log_fatal "must have python3.8 installed"
  fi

  if [[ ! -f "${ROOT_DIR}/.venv/bin/activate" ]]; then
    log_fatal "run setup.sh first"
  fi

  # shellcheck disable=SC1090
  . "${ROOT_DIR}/.venv/bin/activate"

  for input_file in requirements.in dev-requirements.in; do
    pip-compile "${input_file}"
  done
}

main "$@"
