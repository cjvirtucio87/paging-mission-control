"""
Entrypoint for the app.
"""

import sys

import click
from pmc.telemetry import telemetry

@click.command()
@click.argument(
    'filepath',
)
def main(filepath): # pylint: disable=no-value-for-parameter
    """
    A simple telemetry app for parsing telemetry data from
    a file. Alerts will be written to STDOUT in either
    of the following cases:

    - If for the same satellite there are three battery voltage readings
        that are under the red low limit within a five minute interval.

    - If for the same satellite there are three thermostat readings
        that exceed the red high limit within a five minute interval.

    :param filepath: path to the file containing data for ingest
    """
    with open(filepath, encoding='utf8') as file_obj:
        telemetry.scan(file_obj, sys.stdout)

if __name__ == '__main__':
    # click module will take care of the args
    main() # pylint: disable=no-value-for-parameter
