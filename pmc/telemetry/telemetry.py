"""
Module for telemetry functionality.
"""

import datetime
import enum
import json


INPUT_TIMESTAMP_FORMAT = '%Y%m%d %H:%M:%S.%f'
OUTPUT_TIMESTAMP_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


class ColumnIndices(enum.Enum):
    """
    Enumeration for the column indices.
    """
    TIMESTAMP = 0
    SATELLITE_ID = 1
    RED_HIGH_LIMIT = 2
    YELLOW_HIGH_LIMIT = 3
    YELLOW_LOW_LIMIT = 4
    RED_LOW_LIMIT = 5
    RAW_VALUE = 6
    COMPONENT = 7


class ComponentTypes(enum.Enum):
    """
    Enumeration for component types.
    """
    BATTERY = 'BATT'
    THERMOSTAT = 'TSTAT'


class Severity(enum.Enum):
    """
    Enumeration for severity.
    """
    RED_HIGH = 'RED HIGH'
    RED_LOW = 'RED LOW'


def _alert(row, timestamp, output_stream, with_leading_comma):
    satellite_id = int(row[ColumnIndices.SATELLITE_ID.value])
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')

    severity = Severity.RED_HIGH.value \
        if _is_high_temp(row) \
        else Severity.RED_LOW.value

    json_dump = json.dumps(
        {
            'satelliteId': satellite_id,
            'severity': severity,
            'component': component,
            'timestamp': timestamp.strftime(OUTPUT_TIMESTAMP_FORMAT),
        },
        indent=2,
    )
    indented_json_dump = ''

    if with_leading_comma:
        output_stream.write(',')

    for line in json_dump.splitlines():
        indented_json_dump += f"\n  {line}"

    output_stream.write(indented_json_dump)


def _alerted(alerts_data, row, interval_start):
    satellite_id = int(row[ColumnIndices.SATELLITE_ID.value])
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')
    interval_start_str = interval_start.isoformat()

    if satellite_id in alerts_data and \
        interval_start_str in alerts_data[satellite_id] and \
        component in alerts_data[satellite_id][interval_start_str]:
        return True

    if not satellite_id in alerts_data:
        alerts_data[satellite_id] = {}

    if not interval_start_str in alerts_data[satellite_id]:
        alerts_data[satellite_id][interval_start_str] = {}

    if not component in alerts_data[satellite_id][interval_start_str]:
        alerts_data[satellite_id][interval_start_str][component] = True

    return False

def _is_high_temp(row):
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')
    raw_value = float(row[ColumnIndices.RAW_VALUE.value])
    red_high_limit = int(row[ColumnIndices.RED_HIGH_LIMIT.value])

    return component == ComponentTypes.THERMOSTAT.value and \
        raw_value > red_high_limit


def _is_low_batt(row):
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')
    raw_value = float(row[ColumnIndices.RAW_VALUE.value])
    red_low_limit = int(row[ColumnIndices.RED_LOW_LIMIT.value])

    return component == ComponentTypes.BATTERY.value and \
        raw_value < red_low_limit


def _is_severe(satellite_data, row, interval_start):
    satellite_id = int(row[ColumnIndices.SATELLITE_ID.value])
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')
    interval_start_str = interval_start.isoformat()

    return satellite_id in satellite_data and \
        interval_start_str in satellite_data[satellite_id] and \
        component in satellite_data[satellite_id][interval_start_str] and \
        len(satellite_data[satellite_id][interval_start_str][component]) >= 3


def _record_data(satellite_data, row, interval_start):
    satellite_id = int(row[ColumnIndices.SATELLITE_ID.value])
    component = row[ColumnIndices.COMPONENT.value].rstrip('\n')
    interval_start_str = interval_start.isoformat()

    if not _is_high_temp(row) and not _is_low_batt(row):
        return

    if satellite_id not in satellite_data:
        satellite_data[satellite_id] = {}

    if interval_start_str not in satellite_data[satellite_id]:
        satellite_data[satellite_id][interval_start_str] = {}

    if component not in satellite_data[satellite_id][interval_start_str] or \
        satellite_data[satellite_id][interval_start_str][component] is None:
        satellite_data[satellite_id][interval_start_str][component] = []

    satellite_data[satellite_id][interval_start_str][component].append(row)


def scan(input_stream, output_stream):
    """
    Scan an input stream, writing alerts to the output stream
    in either of the following cases:

    - If for the same satellite there are three battery voltage readings
        that are under the red low limit within a five minute interval.

    - If for the same satellite there are three thermostat readings
        that exceed the red high limit within a five minute interval.

    :param input_stream: the stream for the data ingest
    :param output_stream: the stream where alerts
        will be written to
    """
    interval_start = None
    interval_end = None
    satellite_data = {}
    alerts_data = {}

    output_stream.write('[')
    timestamp = None
    with_leading_comma = False

    for line in input_stream:
        row = line.split('|')
        timestamp = datetime.datetime.strptime(
            row[ColumnIndices.TIMESTAMP.value],
            INPUT_TIMESTAMP_FORMAT,
        )

        if interval_start is None:
            interval_start = timestamp
            interval_end = timestamp + datetime.timedelta(minutes=5)
        elif timestamp > interval_end:
            interval_start = interval_end
            interval_end = timestamp + datetime.timedelta(minutes=5)

        _record_data(satellite_data, row, interval_start)

        if _is_severe(satellite_data, row, interval_start) and \
                not _alerted(alerts_data, row, interval_start):
            _alert(
                row,
                timestamp,
                output_stream,
                with_leading_comma,
            )

            with_leading_comma = True

    output_stream.write('\n]\n')
