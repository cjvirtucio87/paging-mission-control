#!/usr/bin/env bash

set -e

### Setup a virtual environment for this app.
###
### Usage:
###   ./setup.sh

ROOT_DIR="$(dirname "$(readlink --canonicalize "$0")")"
readonly ROOT_DIR

function log_fatal {
  >&2 echo "$@"
  exit 1
}

function main {
  if ! command -v python3.8; then
    log_fatal "must have python3.8 installed"
  fi

  if [[ ! -f "${ROOT_DIR}/.venv/bin/activate" ]]; then
    python3.8 -m venv "${ROOT_DIR}/.venv"
  fi

  # shellcheck disable=SC1090
  . "${ROOT_DIR}/.venv/bin/activate"
  python -m pip install \
    pip \
    wheel \
    setuptools \
    pip-tools --upgrade

  python -m pip install \
    --requirement requirements.txt \
    --requirement dev-requirements.txt
}

main "$@"
