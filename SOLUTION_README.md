# paging-mission-control solution

This is a python solution for the Paging Mission Control problem.

## Requirements

This solution requires `python3.8` and `python3-pip`.

## Getting started

This project has a few helper scripts to smooth out the development process:

1. `setup.sh`: sets up a basic development environment
1. `recompile.sh`: for recompiling requirements

Usage information can be parsed from these scripts with:

```bash
grep '###' <script> | sed -E 's/^###\s*//g'
```

## Running the app

After setting up your development environment with `setup.sh`,
activate your virtual environment with:

```bash
. .venv/bin/activate
```

Run the app from the root of this project with:

```bash
python -m pmc --help
```

Simply pass a path to a file containing telemetry data:

```bash
python -m pmc path/to/telemetry_data.txt
```

## Development

Tests can be run with:

```bash
pytest
```
