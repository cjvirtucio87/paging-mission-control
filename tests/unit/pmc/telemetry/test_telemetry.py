import io
import json
import pkgutil
import sys

from pmc.telemetry import telemetry


def _resource_bytes(name, kind, ext):
    return pkgutil.get_data(__name__, f"resources/{kind}/{name}.{ext}")

def test_scan_writes_alerts_to_output_stream():
    names = [
        'basic0',
        'basic1',
        'basic2',
    ]

    actual = None

    for name in names:
        with io.StringIO(_resource_bytes(name, 'inputs', 'txt').decode('utf-8')) as input_stream:
            output_stream = io.StringIO()
            telemetry.scan(input_stream, output_stream)
            actual = output_stream.getvalue()

        print(f"[test_scan] name: {name}", file=sys.stderr)
        assert _resource_bytes(name, 'expectations', 'json').decode('utf-8').rstrip('\n') == actual.rstrip('\n')
